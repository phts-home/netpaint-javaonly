package com.tsarik.application;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;


/**
 * Abstract application's main frame class.
 * 
 * @version 0.1
 * @author Phil Tsarik
 *
 */
public abstract class AbstractMainFrame extends JFrame {
	
	public AbstractMainFrame(AbstractApplication application) {
		this.application = application;
		addWindowListener(new WindowEventsHandler());
		createComponents();
	}
	
	public abstract void onClose();
	
	
	protected abstract void createComponents();
	
	
	
	protected AbstractApplication application;
	
	
	protected class WindowEventsHandler extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			AbstractMainFrame.this.onClose();
		}
	}
	
}
