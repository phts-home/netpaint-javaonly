package com.tsarik.programs.netpaint;

import javax.swing.DefaultListModel;

import com.tsarik.programs.netpaint.components.Coordinates;
import com.tsarik.programs.netpaint.net.CircleData;
import com.tsarik.programs.netpaint.net.EraserData;
import com.tsarik.programs.netpaint.net.PenData;
import com.tsarik.programs.netpaint.net.RectData;
import com.tsarik.programs.netpaint.net.RefreshData;
import com.tsarik.programs.netpaint.net.SendingData;

public class DataHandler {
	
	private DataHandler(NetPaint owner) {
		this.owner = owner;
	}
	
	private static DataHandler instance;
	
	public static void createInstance(NetPaint owner) {
		if (instance == null) {
			instance = new DataHandler(owner);
		}
	}
	
	public static synchronized DataHandler getInstance() {
		return instance;
	}
	
	public void ApplyData(SendingData data) {
		switch (data.getType()) {
		case SendingData.DATATYPE_REFRESH:
			RefreshData refreshData = (RefreshData)data;
			((DefaultListModel)owner.getMainFrame().getClientList().getModel()).removeAllElements();
			for (int i = 0; i < refreshData.getClientArray().length; i++) {
				((DefaultListModel)owner.getMainFrame().getClientList().getModel()).addElement(refreshData.getClientArray()[i]);
			}
			break;
		case SendingData.DATATYPE_CLEAR:
			owner.getMainFrame().getDrawingArea().clearImage();
			break;
		case SendingData.DATATYPE_PEN:
			PenData penData = (PenData)data;
			Object[] c = penData.getCoordinates();
			System.out.println("DATATYPE_PEN:  " + ((Coordinates)c[0]).getX() + "  " + ((Coordinates)c[0]).getY());
			owner.getMainFrame().getDrawingArea().drawLine(penData.getCoordinates(), penData.getRed(), penData.getGreen(), penData.getBlue(), penData.getPenWidth());
			break;
		case SendingData.DATATYPE_RECT:
			RectData rectData = (RectData)data;
			owner.getMainFrame().getDrawingArea().drawRect(rectData.getCoordinates(), rectData.getRed(), rectData.getGreen(), rectData.getBlue(), rectData.getPenWidth());
			break;
		case SendingData.DATATYPE_CIRCLE:
			CircleData circleData = (CircleData)data;
			owner.getMainFrame().getDrawingArea().drawCircle(circleData.getCoordinates(), circleData.getRed(), circleData.getGreen(), circleData.getBlue(), circleData.getPenWidth());
			break;
		case SendingData.DATATYPE_ERASER:
			EraserData eraserData = (EraserData)data;
			owner.getMainFrame().getDrawingArea().drawEraserLine(eraserData.getCoordinates(), eraserData.getPenWidth());
			break;
		case SendingData.DATATYPE_ACCEPT_CLIENT_CLOSE:
			((DefaultListModel)owner.getMainFrame().getClientList().getModel()).removeAllElements();
			break;
		}
		
	}
	
	private NetPaint owner;
	
	
}
