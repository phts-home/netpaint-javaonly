package com.tsarik.programs.netpaint;

import javax.swing.*;

import com.tsarik.application.*;
import com.tsarik.programs.netpaint.frames.MainFrame;
import com.tsarik.programs.netpaint.net.ConnectionManager;

public class NetPaint extends AbstractApplication {
	
	private NetPaint(String host, String name) {
		Parameters.CON_SERVERHOST = host;
		Parameters.CON_PORT = 4444;
		Parameters.CON_NAME = name;
		Parameters.IMAGE_WIDTH = 400;
		Parameters.IMAGE_HEIGHT = 400;
		
		ConnectionManager.createInstance(this);
		
		DataHandler.createInstance(this);
		
		mainFrame = new MainFrame(this);
		mainFrame.setVisible(true);
	}
	
	public static void main(String[] args) {
		String host = "localhost";
		String name = "User";
		if (args.length >= 1) {
			host = args[0];
			if (args.length >= 2) {
				name = args[1];
			}
		}
		new NetPaint(host, name);
	}
	
	@Override
	public void exit() {
		try {
			ConnectionManager.getInstance().closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
		}
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainFrame.dispose();
	}
	
	public MainFrame getMainFrame() {
		return mainFrame;
	}
	
	MainFrame mainFrame;
	
}
