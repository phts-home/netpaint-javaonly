package com.tsarik.programs.netpaint;

import java.awt.Component;

import javax.swing.JOptionPane;

public class Parameters {
	
	public static final String PROGRAM_NAME = "NetPaint";
	public static final String PROGRAM_VERSION = "0.9.5";
	public static final int PROGRAM_BUILD = 20;
	public static final int PROGRAM_YEAR = 2010;
	public static final String PROGRAM_AUTHOR = "Phil Tsarik";
	
	public static String CON_SERVERHOST;
	public static int CON_PORT;
	public static String CON_NAME;
	
	public static int MAINFRAME_WIDTH = 620;
	public static int MAINFRAME_HEIGHT = 470;
	
	public static int IMAGE_WIDTH;
	public static int IMAGE_HEIGHT;
	
	public static String MESSAGE_CONNECTION_ERROR = "Conection Error";
	public static String MESSAGE_SERVER_ALREADYCREATED = "Server is already created";
	public static String MESSAGE_CLIENT_ALREADYCONNECTED = "Client is already connected";
	public static String MESSAGE_CLEAR_ONLYSERVER = "Image can be cleared only by server";
	
	public static void showErrorMessage(Component parentComponent, String message, String errorMessage) {
		JOptionPane.showMessageDialog(parentComponent, message+"\n\nError message:\n\n"+"<html><textarea cols=20 rows=2>"+errorMessage.replaceAll("[\n\r]", " ")+"</textarea></html>", Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showErrorMessage(Component parentComponent, String message) {
		JOptionPane.showMessageDialog(parentComponent, message, Parameters.PROGRAM_NAME, JOptionPane.ERROR_MESSAGE);
	}
	
	public static void showAbout(Component parentComponent) {
		JOptionPane.showMessageDialog(parentComponent,
				"<html><b>"
					+"<h3>"+Parameters.PROGRAM_NAME+"</h3>"
					+"<br>Version: "+Parameters.PROGRAM_VERSION+"."+Parameters.PROGRAM_BUILD+"</b></html>\n\n"
					+"<html>"
					+"&#169; "+Parameters.PROGRAM_YEAR+" by "+Parameters.PROGRAM_AUTHOR
					+"<br>&nbsp;</html>",
				"About", JOptionPane.INFORMATION_MESSAGE);
	}
	
}
