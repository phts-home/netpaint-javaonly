package com.tsarik.programs.netpaint.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.netpaint.Parameters;
import com.tsarik.programs.netpaint.frames.MainFrame;

public class AboutAction extends AbstractProgramMenuAction {
	
	public AboutAction(MainFrame owner) {
		super(owner, "About...");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		Parameters.showAbout(owner);
	}

}
