package com.tsarik.programs.netpaint.actions;

import javax.swing.AbstractAction;

import com.tsarik.programs.netpaint.frames.MainFrame;

public abstract class AbstractProgramMenuAction extends AbstractAction {
	
	public AbstractProgramMenuAction(String name) {
		super(name);
		this.owner = null;
	}
	
	public AbstractProgramMenuAction(MainFrame owner, String name) {
		super(name);
		this.owner = owner;
	}
	
	protected MainFrame owner;

}
