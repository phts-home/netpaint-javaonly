package com.tsarik.programs.netpaint.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.netpaint.Parameters;
import com.tsarik.programs.netpaint.frames.MainFrame;
import com.tsarik.programs.netpaint.net.ConnectionManager;

public class ClearAction extends AbstractProgramMenuAction {
	
	public ClearAction(MainFrame owner) {
		super(owner, "Clear");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		if (ConnectionManager.getInstance().isServerCreated()) {
			owner.getDrawingArea().sendClear();
			return;
		}
		if ( (!ConnectionManager.getInstance().isClientConnected()) && (!ConnectionManager.getInstance().isServerCreated()) ) {
			owner.getDrawingArea().clearImage();
			return;
		}
		Parameters.showErrorMessage(owner, Parameters.MESSAGE_CLEAR_ONLYSERVER);
		
	}

}
