package com.tsarik.programs.netpaint.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.netpaint.Parameters;
import com.tsarik.programs.netpaint.frames.MainFrame;
import com.tsarik.programs.netpaint.net.ConnectionManager;

public class CloseConnectionAction extends AbstractProgramMenuAction {
	
	public CloseConnectionAction(MainFrame owner) {
		super(owner, "Close Connection");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		try {
			ConnectionManager.getInstance().closeConnection();
		} catch (Exception e) {
			e.printStackTrace();
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_CONNECTION_ERROR, e.getMessage());
		}
	}

}
