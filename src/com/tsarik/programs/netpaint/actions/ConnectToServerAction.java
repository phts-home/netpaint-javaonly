package com.tsarik.programs.netpaint.actions;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import com.tsarik.programs.netpaint.Parameters;
import com.tsarik.programs.netpaint.frames.MainFrame;
import com.tsarik.programs.netpaint.net.ConnectionManager;

public class ConnectToServerAction extends AbstractProgramMenuAction {
	
	public ConnectToServerAction(MainFrame owner) {
		super(owner, "Connect To Server");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		if (ConnectionManager.getInstance().isServerCreated()) {
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_SERVER_ALREADYCREATED);
			return;
		}
		if (ConnectionManager.getInstance().isClientConnected()) {
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_CLIENT_ALREADYCONNECTED);
			return;
		}
		
		String v = JOptionPane.showInputDialog(owner, "Server host", Parameters.CON_SERVERHOST);
		if (v == null) {
			return;
		}
		Parameters.CON_SERVERHOST = v;
		v = JOptionPane.showInputDialog(owner, "Enter your name", Parameters.CON_NAME);
		if (v == null) {
			return;
		}
		Parameters.CON_NAME = v;
		try {
			ConnectionManager.getInstance().connectToServer();
		} catch (Exception e) {
			e.printStackTrace();
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_CONNECTION_ERROR, e.getMessage());
		}
	}

}
