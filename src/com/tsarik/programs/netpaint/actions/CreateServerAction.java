package com.tsarik.programs.netpaint.actions;

import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;

import com.tsarik.programs.netpaint.Parameters;
import com.tsarik.programs.netpaint.frames.MainFrame;
import com.tsarik.programs.netpaint.net.ConnectionManager;

public class CreateServerAction extends AbstractProgramMenuAction {
	
	public CreateServerAction(MainFrame owner) {
		super(owner, "Create Server");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		if (ConnectionManager.getInstance().isServerCreated()) {
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_SERVER_ALREADYCREATED);
			return;
		}
		if (ConnectionManager.getInstance().isClientConnected()) {
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_CLIENT_ALREADYCONNECTED);
			return;
		}
		
		String n = JOptionPane.showInputDialog(owner, "Enter your name", Parameters.CON_NAME);
		if (n == null) {
			return;
		}
		Parameters.CON_NAME = n;
		try {
			ConnectionManager.getInstance().createServer();
		} catch (Exception e) {
			e.printStackTrace();
			Parameters.showErrorMessage(owner, Parameters.MESSAGE_CONNECTION_ERROR, e.getMessage());
		}
	}
	
}
