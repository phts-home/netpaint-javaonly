package com.tsarik.programs.netpaint.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.netpaint.frames.MainFrame;

public class ExitAction extends AbstractProgramMenuAction {
	
	public ExitAction(MainFrame owner) {
		super(owner, "Exit");
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.exit();
	}

}
