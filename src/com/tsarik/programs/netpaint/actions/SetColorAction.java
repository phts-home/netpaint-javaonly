package com.tsarik.programs.netpaint.actions;

import java.awt.Color;
import java.awt.event.ActionEvent;

import com.tsarik.programs.netpaint.frames.MainFrame;

public class SetColorAction extends AbstractProgramMenuAction {
	
	public SetColorAction(MainFrame owner, String title, Color color) {
		super(owner, title);
		this.color = color;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.getDrawingArea().setColor(color);
	}
	
	private Color color;

}
