package com.tsarik.programs.netpaint.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.netpaint.frames.MainFrame;

public class SetPenWidthAction extends AbstractProgramMenuAction {
	
	public SetPenWidthAction(MainFrame owner, String title, int penWidth) {
		super(owner, title);
		this.penWidth = penWidth;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.getDrawingArea().setPenWidth(penWidth);
	}
	
	private int penWidth;

}
