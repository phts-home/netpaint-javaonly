package com.tsarik.programs.netpaint.actions;

import java.awt.event.ActionEvent;

import com.tsarik.programs.netpaint.frames.MainFrame;

public class SetToolAction extends AbstractProgramMenuAction {
	
	public SetToolAction(MainFrame owner, String title, int tool) {
		super(owner, title);
		this.tool = tool;
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		owner.getDrawingArea().setTool(tool);
	}
	
	private int tool;

}
