package com.tsarik.programs.netpaint.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;



public class Canvas extends Component {
	
	public Canvas(int w, int h) {
		setSize(w, h);
		bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		g = bi.getGraphics();
		clear();
	}
	
	@Override
	public void paint(Graphics g) {
		System.out.println("paint");
		g.drawImage(bi, 0, 0, null);
	}
	
	@Override
	public void setSize(int width, int height) {
		this.width = width;
		this.height = height;
		super.setSize(width, height);
	}
	
	public Graphics getImageGraphics() {
		return g;
	}
	
	public BufferedImage getImage() {
		return bi;
	}
	
	public void setImage(BufferedImage image) {
		bi = image;
		g = image.getGraphics();
		repaint();
	}
	
	public void clear() {
		Color c = g.getColor();
		g.setColor(Color.WHITE);
		g.fillRect(0, 0, width, height);
		g.setColor(c);
		repaint();
	}
	
	private int width;
	private int height;
	
	private BufferedImage bi;
	private Graphics g;
	
}

