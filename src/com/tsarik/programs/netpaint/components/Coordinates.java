package com.tsarik.programs.netpaint.components;

import java.io.Serializable;

public class Coordinates implements Serializable {
	
	public Coordinates(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	private int x;
	private int y;
	
}
