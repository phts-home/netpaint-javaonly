package com.tsarik.programs.netpaint.components;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import com.tsarik.programs.netpaint.Parameters;
import com.tsarik.programs.netpaint.net.CircleData;
import com.tsarik.programs.netpaint.net.ClearData;
import com.tsarik.programs.netpaint.net.ConnectionManager;
import com.tsarik.programs.netpaint.net.EraserData;
import com.tsarik.programs.netpaint.net.PenData;
import com.tsarik.programs.netpaint.net.RectData;

public class DrawingArea extends JPanel {
	
	public static final int TOOL_PEN = 1;
	public static final int TOOL_RECT = 2;
	public static final int TOOL_CIRCLE = 3;
	public static final int TOOL_ERASER = 4;
	
	public static final Color COLOR_BACKGROUND = Color.WHITE;
	
	public DrawingArea() {
		canvas = new Canvas(Parameters.IMAGE_WIDTH, Parameters.IMAGE_HEIGHT);
		add(canvas);
		setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
		
		setColor(Color.RED);
		setTool(TOOL_PEN);
		setPenWidth(2);
		
		setPreferredSize(new Dimension(Parameters.IMAGE_WIDTH+10, Parameters.IMAGE_HEIGHT+10));
		MouseAdapter mouseHandler = new MouseHandler();
		addMouseListener(mouseHandler);
		addMouseMotionListener(mouseHandler);
		setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		this.setLayout(null);
	}
	
	public void sendClear() {
		try {
			ConnectionManager.getInstance().sendData(new ClearData());
		} catch (Exception e) {
			e.printStackTrace();
		}
		//clearImage(); // TODO: do some things without connection
	}
	
	public void clearImage() {
		canvas.clear();
	}
	
	public BufferedImage getImage() {
		return canvas.getImage();
	}
	
	public byte[] getImageData() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(canvas.getImage(), "bmp", baos);
		return baos.toByteArray();
	}
	
	public void setImage(BufferedImage image) {
		canvas.setImage(image);
	}
	
	public void setImageData(byte[] data) throws IOException {
		canvas.setImage(ImageIO.read(new ByteArrayInputStream(data)));
		setColor(color);
	}
	
	public Color getColor() {
		return color;
	}
	
	public void setColor(Color color) {
		this.color = color;
		if (tool != TOOL_ERASER) {
			canvas.getImageGraphics().setColor(color);
		}
	}
	
	public int getTool() {
		return tool;
	}
	
	public void setTool(int tool) {
		this.tool = tool;
		if (tool == TOOL_ERASER) {
			canvas.getImageGraphics().setColor(COLOR_BACKGROUND);
		} else {
			canvas.getImageGraphics().setColor(color);
		}
	}
	
	public int getPenWidth() {
		return penWidth;
	}
	
	public void setPenWidth(int penWidth) {
		this.penWidth = penWidth;
	}
	
	public void drawLine(Object[] coordinates, int red, int green, int blue, int penWidth) {
		Graphics g = canvas.getImageGraphics();
		Color oldColor = g.getColor();
		g.setColor(new Color(red, green, blue));
		drawLine(((Coordinates)coordinates[0]).getX(), ((Coordinates)coordinates[0]).getY(), ((Coordinates)coordinates[1]).getX(), ((Coordinates)coordinates[1]).getY(), penWidth);
		g.setColor(oldColor);
		canvas.repaint();
	}
	
	public void drawRect(Object[] coordinates, int red, int green, int blue, int penWidth) {
		Graphics g = canvas.getImageGraphics();
		Color oldColor = g.getColor();
		g.setColor(new Color(red, green, blue));
		drawRect(((Coordinates)coordinates[0]).getX(), ((Coordinates)coordinates[0]).getY(), ((Coordinates)coordinates[1]).getX(), ((Coordinates)coordinates[1]).getY(), penWidth);
		g.setColor(oldColor);
		canvas.repaint();
	}
	
	public void drawCircle(Object[] coordinates, int red, int green, int blue, int penWidth) {
		Graphics g = canvas.getImageGraphics();
		Color oldColor = g.getColor();
		g.setColor(new Color(red, green, blue));
		drawCircle(((Coordinates)coordinates[0]).getX(), ((Coordinates)coordinates[0]).getY(), ((Coordinates)coordinates[1]).getX(), ((Coordinates)coordinates[1]).getY(), penWidth);
		g.setColor(oldColor);
		canvas.repaint();
	}
	
	public void drawEraserLine(Object[] coordinates, int penWidth) {
		Graphics g = canvas.getImageGraphics();
		Color oldColor = g.getColor();
		g.setColor(COLOR_BACKGROUND);
		drawLine(((Coordinates)coordinates[0]).getX(), ((Coordinates)coordinates[0]).getY(), ((Coordinates)coordinates[1]).getX(), ((Coordinates)coordinates[1]).getY(), penWidth);
		g.setColor(oldColor);
		canvas.repaint();
	}
	/*
	public void drawPolygon(Object[] coordinates, int red, int green, int blue, int penWidth) {
		/*int x = coordinates[0].getX();
		int y = coordinates[0].getY();
		Color oldColor = canvas.getImageGraphics().getColor();
		canvas.getImageGraphics().setColor(new Color(red, green, blue));
		drawLine(x, y, x, y, penWidth);
		for (int i = 1; i < coordinates.length; i++) {
			drawLine(x, y, coordinates[i].getX(), coordinates[i].getY(), penWidth);
			x = coordinates[i].getX();
			y = coordinates[i].getY();
			
		}
		canvas.getImageGraphics().setColor(oldColor);
		canvas.repaint();
	}//*/
	
	
	
	private Canvas canvas;
	private Color color;
	private int tool;
	private int penWidth;
	
	
	/*
	 * TODO: width
	 */
	private void drawLine(int x1, int y1, int x2, int y2, int penWidth) {
		Graphics g = canvas.getImageGraphics();
		switch (penWidth) {
		case 2:
			g.drawLine(x1, y1, x2, y2);
			g.drawLine(x1+1, y1, x2+1, y2);
			g.drawLine(x1-1, y1, x2-1, y2);
			g.drawLine(x1, y1+1, x2, y2+1);
			g.drawLine(x1, y1-1, x2, y2-1);
			break;
		case 3:
			g.drawLine(x1, y1, x2, y2);
			g.drawLine(x1+1, y1, x2+1, y2);
			g.drawLine(x1+2, y1, x2+2, y2);
			g.drawLine(x1-1, y1, x2-1, y2);
			g.drawLine(x1-2, y1, x2-2, y2);
			g.drawLine(x1, y1+1, x2, y2+1);
			g.drawLine(x1, y1+2, x2, y2+2);
			g.drawLine(x1, y1-1, x2, y2-1);
			g.drawLine(x1, y1-2, x2, y2-2);
			break;
		default:
			g.drawLine(x1, y1, x2, y2);
		}
	}
	
	private void drawRect(int x1, int y1, int x2, int y2, int penWidth) {
		Graphics g = canvas.getImageGraphics();
		switch (penWidth) {
		case 2:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawRect(x1, y1, x2-x1, y2-y1);
					g.drawRect(x1+1, y1+1, x2-x1-2, y2-y1-2);
				} else {
					g.drawRect(x1, y2, x2-x1, y1-y2);
					g.drawRect(x1+1, y2+1, x2-x1-2, y1-y2-2);
				}
			} else {
				if (y1 <= y2) {
					g.drawRect(x2, y1, x1-x2, y2-y1);
					g.drawRect(x2+1, y1+1, x1-x2-2, y2-y1-2);
				} else {
					g.drawRect(x2, y2, x1-x2, y1-y2);
					g.drawRect(x2+1, y2+1, x1-x2-2, y1-y2-2);
				}
			}
			break;
		case 3:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawRect(x1, y1, x2-x1, y2-y1);
					g.drawRect(x1+1, y1+1, x2-x1-2, y2-y1-2);
					g.drawRect(x1-1, y1-1, x2-x1+2, y2-y1+2);
				} else {
					g.drawRect(x1, y2, x2-x1, y1-y2);
					g.drawRect(x1+1, y2+1, x2-x1-2, y1-y2-2);
					g.drawRect(x1-1, y2-1, x2-x1+2, y1-y2+2);
				}
			} else {
				if (y1 <= y2) {
					g.drawRect(x2, y1, x1-x2, y2-y1);
					g.drawRect(x2+1, y1+1, x1-x2-2, y2-y1-2);
					g.drawRect(x2-1, y1-1, x1-x2+2, y2-y1+2);
				} else {
					g.drawRect(x2, y2, x1-x2, y1-y2);
					g.drawRect(x2+1, y2+1, x1-x2-2, y1-y2-2);
					g.drawRect(x2-1, y2-1, x1-x2+2, y1-y2+2);
				}
			}
			break;
		default:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawRect(x1, y1, x2-x1, y2-y1);
				} else {
					g.drawRect(x1, y2, x2-x1, y1-y2);
				}
			} else {
				if (y1 <= y2) {
					g.drawRect(x2, y1, x1-x2, y2-y1);
				} else {
					g.drawRect(x2, y2, x1-x2, y1-y2);
				}
			}
		}
	}
	
	private void drawCircle(int x1, int y1, int x2, int y2, int penWidth) {
		Graphics g = canvas.getImageGraphics();
		switch (penWidth) {
		case 2:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawOval(x1, y1, x2-x1, y2-y1);
					g.drawOval(x1+1, y1+1, x2-x1-2, y2-y1-2);
				} else {
					g.drawOval(x1, y2, x2-x1, y1-y2);
					g.drawOval(x1+1, y2+1, x2-x1-2, y1-y2-2);
				}
			} else {
				if (y1 <= y2) {
					g.drawOval(x2, y1, x1-x2, y2-y1);
					g.drawOval(x2+1, y1+1, x1-x2-2, y2-y1-2);
				} else {
					g.drawOval(x2, y2, x1-x2, y1-y2);
					g.drawOval(x2+1, y2+1, x1-x2-2, y1-y2-2);
				}
			}
			break;
		case 3:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawOval(x1, y1, x2-x1, y2-y1);
					g.drawOval(x1+1, y1+1, x2-x1-2, y2-y1-2);
					g.drawOval(x1-1, y1-1, x2-x1+2, y2-y1+2);
				} else {
					g.drawOval(x1, y2, x2-x1, y1-y2);
					g.drawOval(x1+1, y2+1, x2-x1-2, y1-y2-2);
					g.drawOval(x1-1, y2-1, x2-x1+2, y1-y2+2);
				}
			} else {
				if (y1 <= y2) {
					g.drawOval(x2, y1, x1-x2, y2-y1);
					g.drawOval(x2+1, y1+1, x1-x2-2, y2-y1-2);
					g.drawOval(x2-1, y1-1, x1-x2+2, y2-y1+2);
				} else {
					g.drawOval(x2, y2, x1-x2, y1-y2);
					g.drawOval(x2+1, y2+1, x1-x2-2, y1-y2-2);
					g.drawOval(x2-1, y2-1, x1-x2+2, y1-y2+2);
				}
			}
			break;
		default:
			if (x1 <= x2) {
				if (y1 <= y2) {
					g.drawOval(x1, y1, x2-x1, y2-y1);
				} else {
					g.drawOval(x1, y2, x2-x1, y1-y2);
				}
			} else {
				if (y1 <= y2) {
					g.drawOval(x2, y1, x1-x2, y2-y1);
				} else {
					g.drawOval(x2, y2, x1-x2, y1-y2);
				}
			}
		}
	}
	
	
	private class MouseHandler extends MouseAdapter {
		
		public MouseHandler() {
			//list = new ArrayList<Coordinates>();
			x = 0;
			y = 0;
		}
		
		@Override
		public void mousePressed(MouseEvent evt) {
			x = evt.getX();
			y = evt.getY();
			switch (tool) {
			case TOOL_PEN:
				
				//list.clear();
				//list.add(new Coordinates(x,y));
				drawLine(x, y, x, y, penWidth);
				try {
					ConnectionManager.getInstance().sendData(new PenData(new Object[]{new Coordinates(x,y), new Coordinates(x,y)}, color.getRed(), color.getGreen(), color.getBlue(), penWidth));
				} catch (Exception e) {
					e.printStackTrace();
				}
				canvas.repaint();
				break;
			case TOOL_RECT:
				oldbi = canvas.getImage();
				bi = new BufferedImage(Parameters.IMAGE_WIDTH, Parameters.IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
				bi.setData(oldbi.copyData(oldbi.getRaster()));
				canvas.setImage(bi);
				canvas.getImageGraphics().setColor(color);
				break;
			case TOOL_CIRCLE:
				oldbi = canvas.getImage();
				bi = new BufferedImage(Parameters.IMAGE_WIDTH, Parameters.IMAGE_HEIGHT, BufferedImage.TYPE_INT_RGB);
				bi.setData(oldbi.copyData(oldbi.getRaster()));
				canvas.setImage(bi);
				canvas.getImageGraphics().setColor(color);
				break;
			case TOOL_ERASER:
				drawLine(x, y, x, y, penWidth);
				try {
					ConnectionManager.getInstance().sendData(new EraserData(new Object[]{new Coordinates(x,y), new Coordinates(x,y)}, penWidth));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			}
		}
		
		@Override
		public void mouseReleased(MouseEvent evt) {
			switch (tool) {
			case TOOL_PEN:
				break;
			case TOOL_RECT:
				canvas.setImage(oldbi);
				canvas.getImageGraphics().setColor(color);
				drawRect(x, y, evt.getX(), evt.getY(), penWidth);
				canvas.repaint();
				try {
					ConnectionManager.getInstance().sendData(new RectData(new Object[]{new Coordinates(x,y), new Coordinates(evt.getX(), evt.getY())}, color.getRed(), color.getGreen(), color.getBlue(), penWidth));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case TOOL_CIRCLE:
				canvas.setImage(oldbi);
				canvas.getImageGraphics().setColor(color);
				drawCircle(x, y, evt.getX(), evt.getY(), penWidth);
				canvas.repaint();
				try {
					ConnectionManager.getInstance().sendData(new CircleData(new Object[]{new Coordinates(x,y), new Coordinates(evt.getX(), evt.getY())}, color.getRed(), color.getGreen(), color.getBlue(), penWidth));
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			case TOOL_ERASER:
				break;
			}
		}
		
		@Override
		public void mouseDragged(MouseEvent evt) {
			switch (tool) {
			case TOOL_PEN:
				drawLine(x, y, evt.getX(), evt.getY(), penWidth);
				try {
					ConnectionManager.getInstance().sendData(new PenData(new Object[]{new Coordinates(x,y), new Coordinates(evt.getX(), evt.getY())}, color.getRed(), color.getGreen(), color.getBlue(), penWidth));
				} catch (Exception e) {
					e.printStackTrace();
				}
				x = evt.getX();
				y = evt.getY();
				canvas.repaint();
				break;
			case TOOL_RECT:
				bi.setData(oldbi.getRaster());
				drawRect(x, y, evt.getX(), evt.getY(), penWidth);
				canvas.repaint();
				break;
			case TOOL_CIRCLE:
				bi.setData(oldbi.getRaster());
				drawCircle(x, y, evt.getX(), evt.getY(), penWidth);
				canvas.repaint();
				break;
			case TOOL_ERASER:
				drawLine(x, y, evt.getX(), evt.getY(), penWidth);
				try {
					ConnectionManager.getInstance().sendData(new EraserData(new Object[]{new Coordinates(x,y), new Coordinates(evt.getX(), evt.getY())}, penWidth));
				} catch (Exception e) {
					e.printStackTrace();
				}
				x = evt.getX();
				y = evt.getY();
				canvas.repaint();
				break;
			}
			
		}
		
		private int x;
		private int y;
		private BufferedImage bi;
		private BufferedImage oldbi;
	}
	
}
