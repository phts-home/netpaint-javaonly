package com.tsarik.programs.netpaint.frames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JSplitPane;

import com.tsarik.application.*;
import com.tsarik.programs.netpaint.*;
import com.tsarik.programs.netpaint.actions.AboutAction;
import com.tsarik.programs.netpaint.actions.ClearAction;
import com.tsarik.programs.netpaint.actions.CloseConnectionAction;
import com.tsarik.programs.netpaint.actions.ConnectToServerAction;
import com.tsarik.programs.netpaint.actions.CreateServerAction;
import com.tsarik.programs.netpaint.actions.ExitAction;
import com.tsarik.programs.netpaint.actions.SetColorAction;
import com.tsarik.programs.netpaint.actions.SetPenWidthAction;
import com.tsarik.programs.netpaint.actions.SetToolAction;
import com.tsarik.programs.netpaint.components.DrawingArea;
import com.tsarik.programs.netpaint.net.ConnectionManager;

public class MainFrame extends AbstractMainFrame {
	
	public MainFrame(NetPaint application) {
		super(application);
		setTitle(Parameters.PROGRAM_NAME);
		setSize(Parameters.MAINFRAME_WIDTH, Parameters.MAINFRAME_HEIGHT);
		
		Dimension scrSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation(scrSize.width/2 - Parameters.MAINFRAME_WIDTH/2, scrSize.height/2 - Parameters.MAINFRAME_HEIGHT/2);
	}
	
	public void onClose() {
		application.exit();
	}
	
	public void exit() {
		application.exit();
	}
	
	public DrawingArea getDrawingArea() {
		return drawingArea;
	}
	
	public JList getClientList() {
		return clientList;
	}
	
	
	private DrawingArea drawingArea;
	private JList clientList;
	
	protected void createComponents() {
		JMenu fileMenu = new JMenu("File");
		fileMenu.add(new ExitAction(this));
		
		JMenu connectionMenu = new JMenu("Connection");
		connectionMenu.add(new CloseConnectionAction(this));
		connectionMenu.addSeparator();
		connectionMenu.add(new CreateServerAction(this));
		connectionMenu.add(new ConnectToServerAction(this));
		
		JMenu drawingMenu = new JMenu("Drawing");
		drawingMenu.add(new ClearAction(this));
		drawingMenu.addSeparator();
		drawingMenu.add(new SetToolAction(this, "Pen", DrawingArea.TOOL_PEN));
		drawingMenu.add(new SetToolAction(this, "Rectangle", DrawingArea.TOOL_RECT));
		drawingMenu.add(new SetToolAction(this, "Circle", DrawingArea.TOOL_CIRCLE));
		drawingMenu.add(new SetToolAction(this, "Eraser", DrawingArea.TOOL_ERASER));
		drawingMenu.addSeparator();
		drawingMenu.add(new SetColorAction(this, "Red", Color.RED));
		drawingMenu.add(new SetColorAction(this, "Green", Color.GREEN));
		drawingMenu.add(new SetColorAction(this, "Blue", Color.BLUE));
		drawingMenu.add(new SetColorAction(this, "Cyan", Color.CYAN));
		drawingMenu.add(new SetColorAction(this, "Magenta", Color.MAGENTA));
		drawingMenu.add(new SetColorAction(this, "Yellow", Color.YELLOW));
		drawingMenu.addSeparator();
		drawingMenu.add(new SetPenWidthAction(this, "Thin", 1));
		drawingMenu.add(new SetPenWidthAction(this, "Normal", 2));
		drawingMenu.add(new SetPenWidthAction(this, "Thick", 3));
		
		JMenu helpMenu = new JMenu("Help");
		helpMenu.add(new AboutAction(this));
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(connectionMenu);
		menuBar.add(drawingMenu);
		menuBar.add(helpMenu);
		
		drawingArea = new DrawingArea();
		
		clientList = new JList();
		clientList.setModel(new DefaultListModel());
		
		JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, drawingArea, clientList);
		splitPane.setDividerLocation(1.0);
		
		setJMenuBar(menuBar);
		this.add(splitPane, BorderLayout.CENTER);
	}
	
	
	
}
