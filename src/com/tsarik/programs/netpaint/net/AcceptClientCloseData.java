package com.tsarik.programs.netpaint.net;

public class AcceptClientCloseData extends SendingData {
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_ACCEPT_CLIENT_CLOSE;
	}
	
}
