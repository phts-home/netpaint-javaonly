package com.tsarik.programs.netpaint.net;

public class AcceptServerCloseData extends SendingData {
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_ACCEPT_SERVER_CLOSE;
	}
	
}
