package com.tsarik.programs.netpaint.net;


public class ClearData extends SendingData {
	
	public ClearData() {
	}
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_CLEAR;
	}
	
}
