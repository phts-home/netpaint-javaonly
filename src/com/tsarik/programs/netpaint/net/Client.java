package com.tsarik.programs.netpaint.net;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {
	
	public Client(Socket socket) throws IOException {
		this.socket = socket;
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public ObjectInputStream getIn() throws IOException {
		if (in == null) {
			in = new ObjectInputStream(socket.getInputStream());
		}
		return in;
	}
	
	public ObjectOutputStream getOut() throws IOException {
		if (out == null) {
			out = new ObjectOutputStream(socket.getOutputStream());
		}
		return out;
	}
	
	public void close() throws IOException {
		in.close();
		out.close();
		socket.close();
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	private Socket socket;
	
	private int id;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private String name;
	
}
