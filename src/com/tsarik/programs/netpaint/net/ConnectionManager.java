package com.tsarik.programs.netpaint.net;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;

import javax.swing.DefaultListModel;

import com.tsarik.programs.netpaint.DataHandler;
import com.tsarik.programs.netpaint.NetPaint;
import com.tsarik.programs.netpaint.Parameters;


/*
 * TODO: remove exceptions on application closing
 * TODO: remove printStackTrace()
 * TODO: remove println()
 */
public class ConnectionManager {
	
	private ConnectionManager(NetPaint owner) {
		this.owner = owner;
		client = null;
		server = null;
		newClientId = 0;
		clientConnected = false;
		serverCreated = false;
	}
	
	private static ConnectionManager instance;
	
	public static void createInstance(NetPaint owner) {
		if (instance == null) {
			instance = new ConnectionManager(owner);
		}
	}
	
	public static synchronized ConnectionManager getInstance() {
		return instance;
	}
	
	public void createServer() throws IOException {
		server = null;
		server = new ServerSocket(Parameters.CON_PORT);
		System.out.println("ServerSocket created");
		
		clients = null;
		clients = new ArrayList<Client>();
		
		observer = new ClientsObserver();
		observer.start();
		System.out.println("ServerSocket created: " + InetAddress.getLocalHost().getHostAddress() + " " + server.getLocalPort());
		serverCreated = true;
		
		connectToServer(InetAddress.getLocalHost().getHostName(),server.getLocalPort());
		
	}
	
	public void connectToServer() throws IOException {
		connectToServer(Parameters.CON_SERVERHOST,Parameters.CON_PORT);
	}
	
	public void closeConnection() throws IOException {
		if (server == null) {
			closeClientConnection();
		} else {
			closeServerConnection();
		}
	}
	
	public boolean isServerCreated() {
		return serverCreated;
	}
	
	public boolean isClientConnected() {
		return clientConnected;
	}
	
	public void sendData(Object data) throws IOException {
		if (server == null) {
			sendDataToServer(data);
		} else {
			sendDataToClients(data);
		}
	}
	
	public String[] getClientNames() {
		if (clients == null) {
			return null;
		}
		checkClientSockets();
		String[] ar = new String[clients.size()];
		for (int i = 0; i < clients.size(); i++) {
			ar[i] = clients.get(i).getName();
		}
		return ar;
	}
	
	private NetPaint owner;
	
	private ServerSocket server;
	private ArrayList<Client> clients;
	private ClientsObserver observer;
	private boolean serverCreated;
	
	private Client client;
	private MessageRecieverFromServer receiverFromServer;
	private boolean clientConnected;
	
	private int newClientId;
	
	
	private void connectToServer(String host, int port) throws IOException, ConnectException {
		client = null;
		client = new Client(new Socket(host,port));
		
		receiverFromServer = new MessageRecieverFromServer();
		receiverFromServer.start();
		
		System.out.println("Socket created");
		
		sendDataToServer(new RequestClientConnectData(Parameters.CON_NAME));
		
		clientConnected = true;
	}
	
	
	/*
	 * TODO: send notification to clients about server closing
	 */
	private void closeServerConnection() throws IOException, ConnectException {
		System.out.println("closeServerConnection()");
		if (server == null) {
			return;
		}
		//System.out.println("closeServerConnection(): interrupt()");
		//watcher.interrupt();
		System.out.println("closeServerConnection(): sendDataToClients");
		sendDataToClients(new RequestServerCloseData());
		/*for (int i = 0; i < clients.size(); i++) {
			clients.get(i).close();
		}*/
		
		System.out.println("closeServerConnection(): clients.size(): " + clients.size());
		(new ServerShutdownProvider()).start();
		//clients.clear();
		//server.close(); // TODO: ?
		//server = null;
		
	}
	
	private void closeClientConnection() throws IOException {
		if (client == null) {
			return;
		}
		System.out.println("closeClientConnection()");
		sendDataToServer(new RequestClientCloseData(client.getId()));
		
		
		System.out.println("Client connection closed");
	}
	
	private void closeClientSocket() throws IOException {
		client.close();
		client = null;
		clientConnected = false;
	}
	
	private void closeServerSocket() throws IOException {
		server.close();
		server = null;
		serverCreated = false;
	}
	
	private void sendDataToClients(Object data) throws IOException {
		System.out.println("sendDataToClients: invoke");
		if (server == null) {
			return;
		}
		System.out.println("sendDataToClients: "+clients.size());
		checkClientSockets();
		for (int i = 0; i < clients.size(); i++) {
			ObjectOutputStream out = clients.get(i).getOut();
			if (data != null) {
				out.writeObject(data);
			}
		}
	}
	
	private void sendDataToClientsExcept(Object data, Client client) throws IOException {
		System.out.println("sendDataToClients: invoke");
		if (server == null) {
			return;
		}
		System.out.println("sendDataToClients: "+clients.size());
		checkClientSockets();
		for (int i = 0; i < clients.size(); i++) {
			if (client.getId() != clients.get(i).getId()) {
				ObjectOutputStream out = clients.get(i).getOut();
				if (data != null) {
					out.writeObject(data);
				}
			}
		}
	}
	
	private void sendDataToClient(Client client, Object data) throws IOException {
		if (server == null) {
			return;
		}
		if (client == null) {
			return;
		}
		if (client.getSocket().isClosed()) {
			return;
		}
		ObjectOutputStream out = client.getOut();
		if (data != null) {
			out.writeObject(data);
		}
	}
	
	private void sendDataToServer(Object data) throws IOException {
		if (client == null) {
			return;
		}
		ObjectOutputStream out = client.getOut();
		out.writeObject(data);
	}
	
	private void checkClientSockets() {
		for (int i = clients.size()-1; i == 0; i--) {
			if (clients.get(i).getSocket().isClosed()) {
				clients.remove(i);
			}
		}
	}
	
	private void removeConnectedClientById(int clientId) throws IOException {
		System.out.println("removeConnectedClientById: clients.size():" + clients.size());
		for (int i = 0; i < clients.size(); i++) {
			System.out.println("removeConnectedClientById: id:" + clients.get(i).getId());
			if (clients.get(i).getId() == clientId) {
				clients.get(i).close();
				clients.remove(i);
				break;
			}
		}
	}
	
	private void removeConnectedClient(Client client) throws IOException {
		System.out.println("removeConnectedClient: clients.size():" + clients.size());
		client.close();
		clients.remove(client);
	}
	
	private class ClientsObserver extends Thread {
		@Override
		public void run() {
			if (server == null) {
				return;
			}
			try {
				while (server != null && !server.isClosed()) {
					System.out.println("Waiting client...");
					Socket connectedSocket = server.accept();
					System.out.println("Client connected");
					
					Client newClient = new Client(connectedSocket);
					
					Object ob = isClientSocketCorrect(newClient);
					if (ob == null) {
						newClient.close();
						continue;
					}
					
					RequestClientConnectData requestData = (RequestClientConnectData)ob;
					int newId = (++newClientId);
					System.out.println("newId == "+newId);
					newClient.setId(newId);
					newClient.setName(requestData.getName());
					clients.add(newClient);
					
					System.out.println("Client created");
					(new MessageRecieverFromClient(newClient)).start();
					System.out.println("Client added to list: "+clients.size());
					
					sendDataToClient(newClient, new InitData(newId, owner.getMainFrame().getDrawingArea().getImageData()));
					sendDataToClients(new RefreshData(getClientNames()));
				}
			} catch (SocketException e) {
				e.printStackTrace();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		private Object isClientSocketCorrect(Client newClient) {
			Object ob = null;
			try {
				ObjectInputStream in = newClient.getIn();
				ob = in.readObject();
				if (ob instanceof SendingData) {
					SendingData data = (SendingData)ob;
					if (data.getType() != SendingData.DATATYPE_REQUEST_CLIENT_CONNECT) {
						return null;
					}
				} else {
					return null;
				}
			}
			catch (SocketException e) {
				e.printStackTrace();
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			return ob;
		}
	}
	
	private class MessageRecieverFromClient extends Thread {
		public MessageRecieverFromClient(Client client) {
			this.client = client;
		}
		
		@Override
		public void run() {
			if (client == null) {
				return;
			}
			try {
				ObjectInputStream in = client.getIn();
				while (client != null) {
					Object ob = in.readObject();
					System.out.println(ob);
					if (ob instanceof SendingData) {
						SendingData data = (SendingData)ob;
						switch (data.getType()) {
						/*case SendingData.DATATYPE_REQUEST_CLIENT_CONNECT:
							RequestClientConnectData requestData = (RequestClientConnectData)data;
							int newId = (++newClientId);
							System.out.println("newId == "+newId);
							client.setId(newId);
							client.setName(requestData.getName());
							sendDataToClient(client, new InitData(newId, owner.getMainFrame().getDrawingArea().getImageData()));
							sendDataToClients(new RefreshData(getClientNames()));
							break;*/
						case SendingData.DATATYPE_REQUEST_CLIENT_CLOSE:
							System.out.println("MessageRecieverFromClient: DATATYPE_REQUEST_CLIENT_CLOSE");
							RequestClientCloseData clientCloseData = (RequestClientCloseData)data;
							System.out.println("id: " + clientCloseData.getClientId());
							System.out.println("MessageRecieverFromClient: DATATYPE_REQUEST_CLIENT_CLOSE: clients.size():" + clients.size());
							sendDataToClient(client, new AcceptClientCloseData());
							System.out.println("MessageRecieverFromClient: DATATYPE_REQUEST_CLIENT_CLOSE: 2");
							removeConnectedClientById(clientCloseData.getClientId());
							System.out.println("MessageRecieverFromClient: DATATYPE_REQUEST_CLIENT_CLOSE: clients.size():" + clients.size());
							System.out.println("MessageRecieverFromClient: DATATYPE_REQUEST_CLIENT_CLOSE: 3");
							sendDataToClients(new RefreshData(getClientNames()));
							System.out.println("MessageRecieverFromClient: DATATYPE_REQUEST_CLIENT_CLOSE: 4");
							return;
						case SendingData.DATATYPE_ACCEPT_SERVER_CLOSE:
							// TODO code?
							System.out.println("MessageRecieverFromClient: DATATYPE_ACCEPT_SERVER_CLOSE: clients.size():" + clients.size());
							removeConnectedClient(client);
							System.out.println("MessageRecieverFromClient: DATATYPE_ACCEPT_SERVER_CLOSE: clients.size():" + clients.size());
							return;
						case SendingData.DATATYPE_CLEAR:	// never will receive
						case SendingData.DATATYPE_PEN:
						case SendingData.DATATYPE_RECT:
						case SendingData.DATATYPE_CIRCLE:
						case SendingData.DATATYPE_ERASER:
							sendDataToClientsExcept(ob, client);
							break;
						default:
							sendDataToClients(ob);
						}
					}
				}
			} 
			catch (SocketException e) {
				e.printStackTrace();
				try {
					removeConnectedClient(client);
				} catch(Exception ee) {
				}
			} 
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		private Client client;
	}
	
	private class ServerShutdownProvider extends Thread {
		
		@Override
		public void run() {
			System.out.println("ServerShutdownProvider: run()");
			while (clients.size() != 0) {
				checkClientSockets();
				System.out.println("ServerShutdownProvider: clients.size():" + clients.size());
			}
			System.out.println("ServerShutdownProvider: clients.size():" + clients.size());
			observer.interrupt();
			/*while (watcher.isAlive()) {
				System.out.println("Checker: watcher.alive");
			}*/
			System.out.println("ServerShutdownProvider: observer interrupted");
			try {
				closeServerSocket();
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("Server connection closed");
			((DefaultListModel)owner.getMainFrame().getClientList().getModel()).removeAllElements();
			for (int i = 0; i < getClientNames().length; i++) {
				((DefaultListModel)owner.getMainFrame().getClientList().getModel()).addElement(getClientNames()[i]);
			}
			System.out.println("ServerShutdownProvider: List updated");
		}
		
	}
	
	
	
	private class MessageRecieverFromServer extends Thread {
		@Override
		public void run() {
			if (client == null) {
				return;
			}
			System.out.println("MessageRecieverFromServer: invoke");
			try {
				ObjectInputStream in = client.getIn();
				if (in == null) {
					return;
				}
				while (true) {
					if (client == null) {
						return;
					}
					System.out.println("MessageRecieverFromServer: while");
					Object ob = in.readObject();
					System.out.println("MessageRecieverFromServer: while 2");
					System.out.println(ob);
					if (ob instanceof SendingData) {
						System.out.println("MessageRecieverFromServer: while if");
						SendingData data = (SendingData)ob;
						System.out.println("MessageRecieverFromServer: data.getType(): " + data.getType());
						System.out.println("MessageRecieverFromServer: while if 2");
						switch (data.getType()) {
						case SendingData.DATATYPE_INIT:
							InitData initData = (InitData)data;
							client.setId(initData.getId());
							((DefaultListModel)owner.getMainFrame().getClientList().getModel()).removeAllElements();
							try {
								owner.getMainFrame().getDrawingArea().setImageData(initData.getImageBytes());
							} catch (IOException e) {
								e.printStackTrace();
							}
							break;
						case SendingData.DATATYPE_ACCEPT_CLIENT_CLOSE:
							System.out.println("MessageRecieverFromServer: while if switch DATATYPE_ACCEPTCLOSE");
							closeClientSocket();
							System.out.println("MessageRecieverFromServer: end");
							break;
						case SendingData.DATATYPE_REQUEST_SERVER_CLOSE:
							sendDataToServer(new AcceptServerCloseData());
							closeClientSocket();	// TODO ?
							((DefaultListModel)owner.getMainFrame().getClientList().getModel()).removeAllElements();
							break;
						default:
							DataHandler.getInstance().ApplyData(data);
						}
					}
				}
			}
			catch (SocketException e) {
				e.printStackTrace();
				try {
					closeClientSocket();
				} catch(Exception ee) {
				}
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	
}
