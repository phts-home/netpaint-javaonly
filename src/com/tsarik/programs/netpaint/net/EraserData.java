package com.tsarik.programs.netpaint.net;


public class EraserData extends SendingData {
	
	public EraserData(Object[] coordinates, int penWidth) {
		this.coordinates = coordinates;
		this.penWidth = penWidth;
	}
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_ERASER;
	}
	
	public Object[] getCoordinates() {
		return coordinates;
	}
	
	public int getPenWidth() {
		return penWidth;
	}
	
	@Override
	public String toString() {
		return "Sent coordinates: " + coordinates;
	}
	
	
	private Object[] coordinates;
	private int penWidth;
}
