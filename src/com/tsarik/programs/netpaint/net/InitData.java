package com.tsarik.programs.netpaint.net;


public class InitData extends SendingData {
	
	public InitData(int id, byte[] imageBytes) {
		this.id = id;
		this.imageBytes = imageBytes;
	}
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_INIT;
	}
	
	public int getId() {
		return id;
	}
	
	public byte[] getImageBytes() {
		return imageBytes;
	}
	
	@Override
	public String toString() {
		return "InitData: image, ";
	}
	
	//private ExBufferedImage image;
	private int id;
	private byte[] imageBytes;

}
