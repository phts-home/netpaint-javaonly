package com.tsarik.programs.netpaint.net;


/*
 * TODO: line coordinates
 */
public class PenData extends SendingData {
	
	public PenData(Object[] coordinates, int red, int green, int blue, int penWidth) {
		this.coordinates = coordinates;
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.penWidth = penWidth;
	}
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_PEN;
	}
	
	public Object[] getCoordinates() {
		return coordinates;
	}
	
	public int getRed() {
		return red;
	}
	
	public int getGreen() {
		return green;
	}
	
	public int getBlue() {
		return blue;
	}
	
	public int getPenWidth() {
		return penWidth;
	}
	
	@Override
	public String toString() {
		return "Sent coordinates: " + coordinates;
	}
	
	
	private Object[] coordinates;
	private int red;
	private int green;
	private int blue;
	private int penWidth;
}
