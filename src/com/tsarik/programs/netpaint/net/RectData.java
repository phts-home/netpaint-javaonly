package com.tsarik.programs.netpaint.net;


/*
 * TODO: corners coordinates
 */
public class RectData extends SendingData {
	
	public RectData(Object[] coordinates, int red, int green, int blue, int penWidth) {
		this.coordinates = coordinates;
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.penWidth = penWidth;
	}
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_RECT;
	}
	
	public Object[] getCoordinates() {
		return coordinates;
	}
	
	public int getRed() {
		return red;
	}
	
	public int getGreen() {
		return green;
	}
	
	public int getBlue() {
		return blue;
	}
	
	public int getPenWidth() {
		return penWidth;
	}
	
	@Override
	public String toString() {
		return "Sent coordinates: " + coordinates;
	}
	
	
	private Object[] coordinates;
	private int red;
	private int green;
	private int blue;
	private int penWidth;
}
