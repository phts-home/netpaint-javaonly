package com.tsarik.programs.netpaint.net;

public class RefreshData extends SendingData {
	
	public RefreshData(String[] clientArray) {
		this.clientArray = clientArray;
	}
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_REFRESH;
	}
	
	public String[] getClientArray() {
		return clientArray;
	}
	
	private String[] clientArray;
	
}
