package com.tsarik.programs.netpaint.net;

public class RequestClientCloseData extends SendingData {
	
	public RequestClientCloseData(int clientId) {
		this.clientId = clientId;
	}
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_REQUEST_CLIENT_CLOSE;
	}
	
	public int getClientId() {
		return clientId;
	}
	
	private int clientId;

}
