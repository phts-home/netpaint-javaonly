package com.tsarik.programs.netpaint.net;

public class RequestClientConnectData extends SendingData {
	
	public RequestClientConnectData(String name) {
		this.name = name;
	}
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_REQUEST_CLIENT_CONNECT;
	}
	
	public String getName() {
		return name;
	}
	
	private String name;
	
}
