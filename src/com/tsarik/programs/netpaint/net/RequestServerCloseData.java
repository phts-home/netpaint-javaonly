package com.tsarik.programs.netpaint.net;

public class RequestServerCloseData extends SendingData {
	
	@Override
	public int getType() {
		return SendingData.DATATYPE_REQUEST_SERVER_CLOSE;
	}
	
}
