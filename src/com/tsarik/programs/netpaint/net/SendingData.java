package com.tsarik.programs.netpaint.net;

import java.io.Serializable;

public abstract class SendingData implements Serializable {
	
	public static final int DATATYPE_REQUEST_CLIENT_CONNECT = 1;
	public static final int DATATYPE_INIT = 2;
	public static final int DATATYPE_REFRESH = 3;
	public static final int DATATYPE_CLEAR = 11;
	public static final int DATATYPE_PEN = 12;
	public static final int DATATYPE_RECT = 13;
	public static final int DATATYPE_CIRCLE = 14;
	public static final int DATATYPE_ERASER = 15;
	public static final int DATATYPE_REQUEST_CLIENT_CLOSE = 21;
	public static final int DATATYPE_ACCEPT_CLIENT_CLOSE = 22;
	public static final int DATATYPE_REQUEST_SERVER_CLOSE = 23;
	public static final int DATATYPE_ACCEPT_SERVER_CLOSE = 24;
	
	public abstract int getType();
	
}
